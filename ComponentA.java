public class ComponentA
{
    private ComponentB componentB;

    private ComponentC componentC;

    public ComponentA(
            ComponentB componentB,
            ComponentC componentC)
    {
        this.componentB = componentB;
        this.componentC = componentC;
    }

    public void doOne()
    {
        System.out.println("A doing task one");
        componentB.doOne();
        componentC.doOne();
    }

    public void doTwo()
    {
        System.out.println("A doing task two");
    }
}
