public class Client
{
    public static void main(String[] args)
    {
        // relations
        ComponentB componentB = new ComponentB();
        ComponentC componentC = new ComponentC();
        ComponentA componentA = new ComponentA(componentB, componentC);
        componentB.setComponentA(componentA);
        componentB.setComponentC(componentC);

        //some code and later invoking
        componentA.doOne();
        componentB.doTwo();
    }
}
