public class ComponentB
{
    ComponentA componentA;

    ComponentC componentC;

    public void doTwo()
    {
        System.out.println("B doing task two");
        componentA.doTwo();
        componentC.doTwo();
    }

    public void doOne()
    {
        System.out.println("B doing task one");
    }

    public void setComponentA(ComponentA componentA)
    {
        this.componentA = componentA;
    }

    public void setComponentC(ComponentC componentC)
    {
        this.componentC = componentC;
    }
}
